# BrainMeshLibrary - A library of 3D tetrahedral mesh for neuro-developmental human brain atlases

* Authors: Anh Phong Tran* <tran.anh at husky.neu.edu>, Qianqian Fang** <q.fang at neu.edu>
  * *Department of Chemical Engineering and **Department of Bioengineering
  * Northeastern University
  * 360 Huntington Ave, Boston, MA 02115
* Version: 0.5.0
* License: GPL v2 or later 
* URL: http://www.mcx.space/brain2mesh

## Introduction 

This library is described in the following paper:

The details of this toolbox can be found in the following paper:
Anh Phong Tran and Qianqian Fang, "Fast and high-quality tetrahedral mesh generation 
from neuroanatomical scans,". In: arXiv pre-print (August 2017). arXiv:1708.08954v1 [physics.med-ph]
URL: https://arxiv.org/abs/1708.08954

## Acknowledgement 

This project is funded by the National Institutes of Health (NIH) / National Institute of General 
Medical Sciences (NIGMS) under the grant number R01-GM114365 and Natonal Cancer Institute (NCI) 
under the grant number R01-CA204443.
